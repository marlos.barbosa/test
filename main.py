from modules.read import read_something
from modules.register import alter_something
from modules.register import delete_something
from modules.register import register_new

stoping_criteria = 1
while stoping_criteria != 2:
    action = int(input("1. Realizar cadastro\n2. Realizar alteração\n3. Realizar deleção\n4. Realizar leitura\n"))
    if action == 1:
        specific_action = int(input("1. Cadastrar professor\n2. Cadastrar curso\n3. Cadastrar matéria\n4. Cadastrar aluno\n5. Cadastrar matrícula\n"))
        if specific_action == 1:
            register_new.new_professor()
            stoping_criteria = int(input("1. Realizar outra ação\n2. Encerrar uso do banco de dados\n"))
        elif specific_action == 2:
            register_new.new_course()
            stoping_criteria = int(input("1. Realizar outra ação\n2. Encerrar uso do banco de dados\n"))
        elif specific_action == 3:
            register_new.new_subject()
            stoping_criteria = int(input("1. Realizar outra ação\n2. Encerrar uso do banco de dados\n"))
        elif specific_action == 4:
            register_new.new_student()
            stoping_criteria = int(input("1. Realizar outra ação\n2. Encerrar uso do banco de dados\n"))
        elif specific_action == 5:
            register_new.new_enrollment()
            stoping_criteria = int(input("1. Realizar outra ação\n2. Encerrar uso do banco de dados\n"))
    elif action == 2:
        specific_action = int(input("1. Alterar curso de aluno\n2. Alterar professor da disciplina\n"))
        if specific_action == 1:
            alter_something.alter_course()
            stoping_criteria = int(input("1. Realizar outra ação\n2. Encerrar uso do banco de dados\n"))
        elif specific_action == 2:
            alter_something.alter_professor()          
            stoping_criteria = int(input("1. Realizar outra ação\n2. Encerrar uso do banco de dados\n"))
    elif action == 3:
        specific_action = int(input("1. Deletar matrícula\n"))
        if specific_action == 1:
            delete_something.delete_enrollment()
            stoping_criteria = int(input("1. Realizar outra ação\n2. Encerrar uso do banco de dados\n"))
    elif action == 4:
        specific_action = int(input("1. Ler informações de todos os estudantes\n2. Ler curso e matérias de um estudante\n3. Ler nome de todos os estudantes de um curso\n4. Ler todos os estudantes de uma matéria\n5. Ler ranking de matérias de acordo com o número de alunos matriculados\n"))
        if specific_action == 1:
            read_something.read_all_students_info()
            stoping_criteria = int(input("1. Realizar outra ação\n2. Encerrar uso do banco de dados\n"))
        elif specific_action == 2:
            read_something.read_one_student_course_and_subjects()
            stoping_criteria = int(input("1. Realizar outra ação\n2. Encerrar uso do banco de dados\n"))
        elif specific_action == 3:
            read_something.all_students_from_a_course()
            stoping_criteria = int(input("1. Realizar outra ação\n2. Encerrar uso do banco de dados\n"))
        elif specific_action == 4:
            read_something.all_students_from_a_subject()
            stoping_criteria = int(input("1. Realizar outra ação\n2. Encerrar uso do banco de dados\n"))
        elif specific_action == 5:
            read_something.ranking_by_number_of_enrolled_students()
            stoping_criteria = int(input("1. Realizar outra ação\n2. Encerrar uso do banco de dados\n"))
        


            
