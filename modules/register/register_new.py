from uuid import uuid4
import sqlite3

def new_professor():
    connection = sqlite3.connect("./database.db")
    client = connection.cursor()
    professor_id = str(uuid4())
    professor_cpf = str(input("CPF do professor: "))
    professor_name = str(input("Nome do professor: "))
    academic_degree = str(input("Grau acadêmico do professor: "))
    client.execute(f'INSERT INTO PROFESSORES (professor_id, CPF, professor_name, professor_academic_degree) values("{professor_id}","{professor_cpf}","{professor_name}","{academic_degree}");')
    connection.commit()

def new_course():
    connection = sqlite3.connect("./database.db")
    client = connection.cursor()
    course_id = str(uuid4())
    course_name = str(input("Nome do curso: "))
    creation_date = int(input("Data de criação do curso: "))
    building_name = str(input("Nome do prédio: "))
    coordinator_name = str(input("Nome do coordenador: "))
    coordinator_id = client.execute(f'SELECT professor_id FROM PROFESSORES WHERE professor_name = "{coordinator_name}";')
    coordinator_id = client.fetchone()
    if coordinator_id != None:
        coordinator_id = coordinator_id[0]
        client.execute(f'INSERT INTO CURSOS (course_id, coordinator_id, course_name, creation_date, building_name) VALUES("{course_id}","{coordinator_id}","{course_name}","{creation_date}","{building_name}")')
        connection.commit()
    else:
        print("Realize o cadastro do coordenador antes de cadastrar o curso!")


def new_subject():
    connection = sqlite3.connect("./database.db")
    client = connection.cursor()
    subject_id = str(uuid4())
    professor_name = str(input("Nome do professor: "))
    subject_name = str(input("Nome da disciplina: "))
    code = str(input("Código da disciplina: "))
    description = str(input("Descrição da disciplina: "))
    professor_id = client.execute(f'SELECT professor_id FROM PROFESSORES WHERE professor_name = "{professor_name}";')
    professor_id = client.fetchone()
    if professor_id != None:
        professor_id = professor_id[0]
        client.execute(f'INSERT INTO DISCIPLINAS (subject_id,professor_id,subject_name,code,description) VALUES("{subject_id}","{professor_id}","{subject_name}","{code}","{description}")')
        connection.commit()
    else:
        print("Realize o cadastro do professor antes de cadastrar a disciplina!")

def new_student():
    connection = sqlite3.connect("./database.db")
    client = connection.cursor()
    student_id = str(uuid4())
    course_name = str(input("Curso do estudante: "))
    student_cpf = str(input("CPF do aluno: "))
    student_name = str(input("Nome do aluno: "))
    birthday_date = str(input("Data de aniversário do estudante: "))
    student_rg = str((input("RG do estudante: ")))
    dispatching_agency = str((input("Órgão expedidor: ")))
    course_id = client.execute(f'SELECT course_id FROM CURSOS WHERE course_name = "{course_name}";')
    course_id = client.fetchone()
    if course_id != None:
        course_id = course_id[0]
        client.execute(f'INSERT INTO ALUNOS (student_id,course_id,CPF,student_name,birthday_date,RG,dispatching_agency) VALUES ("{student_id}","{course_id}","{student_cpf}","{student_name}","{birthday_date}","{student_rg}","{dispatching_agency}")')
        connection.commit()
    else:
        print("Cadastre o curso antes de cadastro os alunos!")

def new_enrollment():
    connection = sqlite3.connect("./database.db")
    client = connection.cursor()
    student_cpf = str(input("CPF do estudante: "))
    subject_name = str(input("Nome da matéria: "))
    student_id = client.execute(f'SELECT student_id FROM ALUNOS WHERE CPF = "{student_cpf}";')
    student_id = client.fetchone()
    if student_id!=None:
        student_id = student_id[0]
        subject_id = client.execute(f'SELECT subject_id FROM DISCIPLINAS WHERE subject_name = "{subject_name}";')
        subject_id = client.fetchone()
        if subject_id !=None:
            subject_id = subject_id[0]
            client.execute(f'INSERT INTO ALUNOS_DISCIPLINAS (student_id,subject_id) VALUES ("{student_id}","{subject_id}")')
            connection.commit()
        else:
            print("Cadastre a matéria antes realizar a  matrícula!")
    else:
        print("Cadastre o aluno antes de realizar a matrícula!")
