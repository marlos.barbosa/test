import sqlite3
def delete_enrollment():
    connection = sqlite3.connect("./database.db")
    client = connection.cursor()
    student_cpf = str(input("CPF do estudante: "))
    subject_name = str(input("Nome da disciplina: "))
    student_id = client.execute(f'SELECT student_id FROM ALUNOS WHERE CPF = "{student_cpf}";')
    student_id = client.fetchone()
    if student_id != None:
        student_id = student_id[0]
        subject_id = client.execute(f'SELECT subject_id FROM DISCIPLINAS WHERE subject_name = "{subject_name}";')
        subject_id = client.fetchone()
        if subject_id != None:
            subject_id = subject_id[0]
            client.execute(f'DELETE FROM ALUNOS_DISCIPLINAS WHERE student_id = "{student_id}" AND subject_id = "{subject_id}"')
            connection.commit()
        else:
            print("Não há nenhuma disciplina com esse nome cadastrada!")
    else:
        print("Não há nenhum aluno com esse CPF cadastrado!")