import sqlite3
def alter_course():
    connection = sqlite3.connect("./database.db")
    client = connection.cursor()
    student_cpf = str(input("CPF do estudante: "))
    new_course = str(input("Nome do novo curso: "))
    course_id = client.execute(f'SELECT course_id FROM CURSOS WHERE course_name = "{new_course}"')
    course_id = client.fetchone()
    if course_id != None:
        course_id = course_id[0]
        client.execute(f'UPDATE ALUNOS SET course_name = "{course_id}" WHERE CPF = "{student_cpf}"')
        connection.commit()
    else:
        print("Não há curso com esse nome!")

def alter_professor():
    connection = sqlite3.connect("./database.db")
    client = connection.cursor()
    new_professor_cpf = str(input("CPF do novo professor: "))
    old_professor_cpf = str(input("CPF do antigo professor: "))
    subject_name = str(input("Nome da disciplina: "))
    new_professor_id = client.execute(f'SELECT professor_id FROM PROFESSORES WHERE CPF = "{new_professor_cpf}"')
    new_professor_id = client.fetchone()
    if new_professor_id != None:                
        new_professor_id = new_professor_id[0]
        old_professor_id = client.execute(f'SELECT professor_id FROM PROFESSORES WHERE CPF = "{old_professor_cpf}"')
        old_professor_id = client.fetchone()
        if old_professor_id != None:
            old_professor_id = old_professor_id[0]
            client.execute(f'UPDATE DISCIPLINAS SET professor_id = "{new_professor_id}" WHERE (subject_name = "{subject_name}" AND professor_id = "{old_professor_id}")')
            connection.commit()
        else:
            print("Não há nenhum professor antigo cadastrado com esse CPF!")
    else:
        print("O professor novo ainda não foi cadastrado!")