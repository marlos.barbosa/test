import sqlite3

def read_all_students_info():
    connection = sqlite3.connect("./database.db")
    client = connection.cursor()
    client.execute(f'SELECT course_id,student_id,CPF,student_name,birthday_date,RG,dispatching_agency FROM ALUNOS')
    student_data = client.fetchall()
    if len(student_data) !=0:
        for i in range(0,len(student_data)):
            client.execute(f'SELECT course_name FROM CURSOS WHERE course_id ="{student_data[i][0]}"')
            students_courses = client.fetchone()
            if students_courses !=None:
                print("student_id: ", student_data[i][1])
                print("CPF: ", student_data[i][2])
                print("student_name: ", student_data[i][3])
                print("birthday_date: ", student_data[i][4])
                print("RG: ", student_data[i][5])
                print("dispatching_agency: ", student_data[i][6])
                print("course_name: ", students_courses[0])
                print("\n")
            else:
                print("Não há dados para realizar essa leitura!")
    else:
        print("Não há dados para realizar essa leitura!")

def read_one_student_course_and_subjects():
    connection = sqlite3.connect("./database.db")
    client = connection.cursor()
    student_cpf = str(input("CPF do estudante: "))
    student_id = client.execute(f'SELECT student_id FROM ALUNOS WHERE CPF = "{student_cpf}";')
    student_id = client.fetchone()
    if student_id!=None:
        student_id = student_id[0]
        course_id = client.execute(f'SELECT course_id FROM ALUNOS WHERE CPF = "{student_cpf}";')
        course_id = client.fetchone()
        if course_id != None:
            course_id = course_id[0]
            client.execute(f'SELECT course_name FROM CURSOS WHERE course_id = "{course_id}"')
            course = client.fetchone()
            if course !=None:
                course = course[0]
                print(course)
                print("\n")
                client.execute(f'SELECT subject_id FROM ALUNOS_DISCIPLINAS WHERE student_id = "{student_id}"')
                subjects = client.fetchall()
                if subjects!=None:
                    for i in range(0,len(subjects)):
                        client.execute(f'SELECT subject_name FROM DISCIPLINAS WHERE subject_id="{subjects[i][0]}"')
                        subject=client.fetchone()
                        if subject!=None:
                            subject=subject[0]
                            print(subject)
                        else:
                            print("Não há dados para realizar essa leitura!")
                else:
                    print("Não há dados para realizar essa leitura!")
            else:
                print("Não há dados para realizar essa leitura!")
        else:
            print("Não há dados para realizar essa leitura!")
    else:
        print("Não há dados para realizar essa leitura!")

def all_students_from_a_course():
    connection = sqlite3.connect("./database.db")
    client = connection.cursor()
    read_course_name = str(input("Nome do curso: "))
    course_id = client.execute(f'SELECT course_id FROM CURSOS WHERE course_name ="{read_course_name}"')
    course_id = client.fetchone()
    if course_id !=None:
        course_id = course_id[0]
        students = client.execute(f'SELECT student_name FROM ALUNOS WHERE course_id = "{course_id}"')
        students = client.fetchall()
        if students!=None:
            for i in range(0,len(students)):
                print(students[i][0])
        else:
            print("Não há dados para realizar essa leitura!")
    else:
        print("Não há dados para realizar essa leitura!")

def all_students_from_a_subject():
    connection = sqlite3.connect("./database.db")
    client = connection.cursor()
    read_subject_name = str(input("Nome da disciplina: "))
    subject_id = client.execute(f'SELECT subject_id FROM DISCIPLINAS WHERE subject_name = "{read_subject_name}"')
    subject_id = client.fetchone()
    if subject_id!=None:
        subject_id = subject_id[0]
        student_id = client.execute(f'SELECT student_id FROM ALUNOS_DISCIPLINAS WHERE subject_id = "{subject_id}"')
        student_id = client.fetchall()
        if student_id!=None:
            for i in range(0,len(student_id)):
                student_name = client.execute(f'SELECT student_name FROM ALUNOS WHERE student_id = "{student_id[i][0]}"')
                student_name = client.fetchone()
                if student_name!=None:
                    print(student_name[0])
                else:
                    print("Não há dados para realizar essa leitura!")
        else:
            print("Não há dados para realizar essa leitura!")
    else:
        print("Não há dados para realizar essa leitura!")

def ranking_by_number_of_enrolled_students():
    connection = sqlite3.connect("./database.db")
    client = connection.cursor()
    all_subjects = client.execute(f'SELECT DISTINCT subject_id FROM ALUNOS_DISCIPLINAS')
    all_subjects = client.fetchall()
    list_name = []
    list_quantity=[]
    for i in range(0,len(all_subjects)):
        student_quantity = client.execute(f'SELECT COUNT (student_id) FROM ALUNOS_DISCIPLINAS WHERE subject_id = "{all_subjects[i][0]}"')
        student_quantity = client.fetchall()
        subjects_name = client.execute(f'SELECT subject_name FROM DISCIPLINAS WHERE subject_id = "{all_subjects[i][0]}"')
        subjects_name = client.fetchall()
        list_quantity.append(student_quantity[0][0])
        list_name.append(subjects_name[0][0]) 
    if len(list_quantity)!=0 and len(list_name)!=0:
        list_quantity,list_name = (list(t) for t in zip(*sorted(zip(list_quantity,list_name))))
        for i in range(len(list_quantity)-1,-1,-1):
            print(f'Disciplina: {list_name[i]}\nQuantidade de alunos: {list_quantity[i]}')
    else:
        print("Não há dados para realizar essa leitura!")