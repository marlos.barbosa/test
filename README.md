# SQLITE3 s2 Python

Abra o terminal Bash (Git Bash) e digite, no diretório my-first-api:

```bash
pip install pipenv
export PIPENV_VENV_IN_PROJECT="enabled"
mkdir .venv
pipenv shell
pipenv install
source .venv/Scripts/activate
pipenv run start
```

